#!/usr/bin/bash
#PBS -j oe
#PBS -N mnist_dcgan
#PBS -M jo7ueb@gmail.com
#PBS -m be

cd $PBS_O_WORKDIR
module swap cuda/10.0

poetry run python train.py --device 0 --epoch 250 --n_hidden 20 --seed 1115
